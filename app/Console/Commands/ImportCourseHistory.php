<?php

namespace App\Console\Commands;

use App\Jobs\SaveCourseExchangeHistoryJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class ImportCourseHistory extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:current-currencies';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get current course data';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        SaveCourseExchangeHistoryJob::dispatch(Carbon::now())->afterCommit();

        exit('imported');
    }
}
