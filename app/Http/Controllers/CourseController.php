<?php

namespace App\Http\Controllers;


use App\Http\Requests\HistoryFilterRequest;
use App\Http\Resources\CourseResource;
use App\Interfaces\CourseExchangeContract;
use App\Services\CourseExchangeService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CourseController extends Controller
{
    /**
     * @var CourseExchangeContract //todo Contract
     */
    protected CourseExchangeService $exchangeService;

    /**
     * @param CourseExchangeContract $exchangeService
     */
    public function __construct(CourseExchangeContract $exchangeService)
    {
        $this->exchangeService = $exchangeService;
    }

    /**
     * @param HistoryFilterRequest $request
     * @return AnonymousResourceCollection
     */
    public function index(HistoryFilterRequest $request): AnonymousResourceCollection
    {
        $histories = $this->exchangeService->getHistory(
            $request->only(['start_date', 'end_date'])
        );

        return CourseResource::collection($histories);
    }

    /**
     * @return JsonResponse|CourseResource
     */
    public function current(): JsonResponse|CourseResource
    {
        $courses = $this->exchangeService->getCurrent();

        return CourseResource::make($courses);
    }
}
