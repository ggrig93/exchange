<?php

namespace App\Interfaces;

use App\Models\ExchangeHistory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface CourseExchangeContract
{
    /**
     * @return ExchangeHistory|null
     */
    public function getCurrent(): Model|null;


    /**
     * @param array $filters
     * @return Collection
     */
    public function getHistory(array $filters = []): Collection;
}
