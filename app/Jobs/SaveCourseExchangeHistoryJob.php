<?php

namespace App\Jobs;

use App\Models\ExchangeHistory;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SaveCourseExchangeHistoryJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    const MAX_ATTEMPTS = 10;
    const RETRY_AFTER = 180;

    /**
     * @var Carbon
     */
    public $date;

    /**
     * Create a new job instance.
     */
    public function __construct(Carbon $date)
    {
        $this->date = $date;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {

        try {
            $data = json_decode(file_get_contents(config('currency.host') . 'daily_json.js'), true);
            $course = ExchangeHistory::query()->whereDate('created_at', $this->date)->first();
            if($course) {
                return;
            }

            if (!isset($data['error'])) {
                ExchangeHistory::query()->create([
                    'date' => Carbon::parse($data['Date']),
                    'valute' => $data['Valute'],
                ]);
            } else {

                /**
                 * https://www.cbr-xml-daily.ru/daily_json.js have json files https://www.cbr-xml-daily.ru/archive/2023/08/12/daily_json.js
                 *  IN case API does not return data for weekend, use Friday's results to have data for all days
                 */
                if (in_array( $this->date->dayOfWeek, [Carbon::SUNDAY, Carbon::SATURDAY])) {
                    $lastHistory = ExchangeHistory::query()->orderByDesc('created_at')->first();
                    $lastHistory->duplicateFridayRecord();
                }
            }

        } catch (\Throwable $exception) {
            if ($this->attempts() > self::MAX_ATTEMPTS) {
                // fail after 100 attempts
                throw $exception;
            }

            // requeue this job to be executes
            // in 3 minutes (180 seconds) from now
            $this->release(self::RETRY_AFTER);
            return;
        }

    }

    /**
     * @return Carbon
     */
    public function retryUntil(): Carbon
    {
        // will keep retrying, by backoff logic below
        // until 12 hours from first run.
        return now()->addHours(12);
    }
}
