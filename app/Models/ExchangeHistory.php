<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property Carbon $date
 * @property array $valute
 * @property bool $friday_record
 * @method static Builder|ExchangeHistory filter($filter)
 */
class ExchangeHistory extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'date',
        'valute',
        'friday_record'
    ];
    /**
     * @var string[]
     */
    protected $casts = [
        'valute' => 'json',
        'date' => 'datetime',
    ];

    /**
     * @param Builder $query
     * @param array $filter
     * @return void
     */
    public function scopeFilter(Builder $query, array $filter): void
    {
        $query->when(isset($filter['start_date']), function ($query) use ($filter) {
            $query->whereDate('created_at', '>=', $filter['start_date']);
        })->when(isset($filter['end_date']), function ($query) use ($filter) {
            $query->whereDate('created_at', '<=', $filter['end_date']);
        });
    }


    /**
     * @return ExchangeHistory|Model
     */
    public function duplicateFridayRecord(): ExchangeHistory|Model
    {
        $data = $this->toArray();
        $data['date'] = Carbon::parse($data['date'])->addDay();
        $data['friday_record'] = true;
        return self::query()->create($data);
    }
}
