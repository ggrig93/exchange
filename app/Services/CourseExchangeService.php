<?php

namespace App\Services;

use App\Interfaces\CourseExchangeContract;
use App\Models\ExchangeHistory;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class CourseExchangeService implements CourseExchangeContract
{
    /**
     * @return Model|null
     */
    public function getCurrent(): Model|null
    {
       return ExchangeHistory::query()->orderByDesc('created_at')->first();
    }

    /**
     * @param $filters
     * @return Collection
     */
    public function getHistory($filters = []): Collection
    {
        return ExchangeHistory::query()->filter($filters)->get();
    }
}
